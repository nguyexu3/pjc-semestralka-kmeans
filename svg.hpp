#pragma once
#include "point.hpp"
#include <vector>
#include <iostream>
class svg
{
public:
    void generate(const std::vector<point> &classifications, const std::vector<point> &centroids, std::string output);

private:
    static const int padding = 50;
    static const int width = 800;
    static const int height = 500;
    static const int chartWidth = width - padding * 2;
    static const int chartHeight = height - padding * 2;
    static const int fontSize = 18;
    static const int numHorizontalGuides = 8;
    static const int numVerticalGuides = 8;
    const std::string colors[8] = {"red", "yellow", "blue", "green", "purple", "brown", "orange", "pink"};

    double maximumY(const std::vector<point> &data) const;
    double maximumX(const std::vector<point> &data) const;
    void write(std::ofstream &out, const std::vector<point> &classifications, const std::vector<point> &centroids);
    void chart_axis(std::ofstream &out, int x1, int y1, int x2, int y2);
    void chart_point(std::ofstream &out, point point, double maxX, double maxY, std::string color);
    void chart_xaxis_labels(std::ofstream &out, double maxX);
    void chart_yaxis_labels(std::ofstream &out, double maxY);
    void chart_vertical_guides(std::ofstream &out);
    void chart_horizontal_guides(std::ofstream &out);
};

# PJC semestralka: KMeans/Clustering

## Zadani

K-means je jednoduchý iterativní algoritmus pro shlukování množiny bodů do k skupin (kde k je známé předem) s jednoduchou paralelizací. Vstupem je množina bodů (standardní vstup nebo soubor) a počet skupin (parametr na příkazové řádce), na výstupu vrací seznam bodů a jejich příslušnost do skupin.

## Implemetace

Program vyuziva algoritmus kmeans, kde dostavame data bodu(kvuli implementaci svg jsme limitovani na 2 rozmerny prostor) s x a y koordinaci. Tyto data proiterujeme a pridelime nejblizsi centroid (bod clusteru). Centroidy jsou ze zacatky maji hodnoty prvni k dat, castejsi implemtaci je taky vybrat nahodnych k bodu z dat. Po te spocitame nove koordinace centroidu do te doby kdy se neprestane hybat, tim omptimalizujeme algoritmus.

K vygenerovani vysledku jsem si vytvoril vlastni svg render. Graf svg je schvalne rozdeleny na stejne kusy pro lepsi vyzualizaci.

Po analyze algoritmu jsem nenasel smysuplnou cast ktera by se dala paralezovat tak jsem misto toho implementoval multithredovani pri nekolika soubeznych evaluaci.

## Pouziti

Pro spousteni programu je potreba stupni data v textovem souboru (Priklad takoveho souboru najdete v `/sample_data`). Kazdy bod ma vlasti radek.

Povinne argumenty spousteni programu jsou `-i` a `-k`, kde k je pocet skupin/clusteru ktere chceme identifikovat.

Vystup tohoto programu je vygenerovany svg soubor s evaluanym grafem, priklad vystupu najdete na `/sample_result`.

Program dokaze spracovat na vstupu nekolik souboru najdenou, nazvy souboru oddelujeme carkou. Napr: `-i sample_1.txt,sample_2.txt,sample_3.txt`

Pro zapnuti paralelniho zpracovani pouzijem argument `-mt`, defaultne je aplikace single threaded

## Vykon

**sample_1 - pro 2 clustery**
single_thread: 0ms
multi_thread: 0ms

**sample_1, sample_2 pro 2 clustery**
single_thread: 6ms
multi_thread: 4ms

**sample_1, sample_2, sample_3 pro 2 clustery**
single_thread: 9ms
multi_thread: 6ms;

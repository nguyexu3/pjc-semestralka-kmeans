#pragma once
#include "point.hpp"
#include <vector>

class kmeans
{
public:
    std::vector<point> classifications;
    std::vector<point> centroids;

    void evaluate(const int k, std::vector<point> data);

private:
    static const int max_iteration = 20;
};

#include "svg.hpp"
#include <fstream>
#include <iomanip>
#include <float.h>

void svg::generate(const std::vector<point> &classifications, const std::vector<point> &centroids, std::string output)
{
    std::ofstream out(output);
    write(out, classifications, centroids);
    out.close();
}

double svg::maximumX(const std::vector<point> &data) const
{
    double max = DBL_MIN;
    for (const point &p : data)
    {
        if (p.x > max)
        {
            max = p.x;
        }
    }
    return max;
}

double svg::maximumY(const std::vector<point> &data) const
{
    double max = DBL_MIN;
    for (const point &p : data)
    {
        if (p.y > max)
        {
            max = p.y;
        }
    }
    return max;
}

void svg::write(std::ofstream &out, const std::vector<point> &classifications, const std::vector<point> &centroids)
{
    double maxX = maximumX(classifications);
    double maxY = maximumY(classifications);

    out << "<?xml version=\"1.0\" encoding='UTF-8' ?>\n"
        << "<svg xmlns='http://www.w3.org/2000/svg' width='"
        << width << "' height='"
        << height << "'>\n";

    chart_axis(out, padding, height - padding, width - padding, height - padding);
    chart_axis(out, padding, padding, padding, height - padding);
    chart_vertical_guides(out);
    chart_horizontal_guides(out);
    chart_xaxis_labels(out, maxX);
    chart_yaxis_labels(out, maxY);
    for (const point &p : classifications)
    {
        chart_point(out, p, maxX, maxY, colors[p.cluster]);
    }
    for (const point &p : centroids)
    {
        chart_point(out, p, maxX, maxY, "black");
    }

    out << "</svg>";
}

void svg::chart_axis(std::ofstream &out, int x1, int y1, int x2, int y2)
{
    out << "<polyline fill='none' stroke='#ccc' strokeWidth='.5' points='" << x1 << "," << y1 << " " << x2 << "," << y2 << "' />\n";
}

void svg::chart_point(std::ofstream &out, point point, double maxX, double maxY, std::string color)
{
    int x = (point.x / maxX) * chartWidth + padding;
    int y = chartHeight - (point.y / maxY) * chartHeight + padding;
    out << "<circle  stroke='black' stroke-width='1' r='5' fill='" << color << "' cx='" << x << "' cy='" << y << "'/>\n";
}

void svg::chart_xaxis_labels(std::ofstream &out, double maxX)
{
    int y = height - padding + fontSize * 2;

    for (int i = 1; i < maxX + 1; i++)
    {
        double ratio = (i / (double)numVerticalGuides);
        int x = ratio * chartWidth + padding - fontSize / 2;
        out << "<text x='" << x << "' y='" << y << "' font-size='" << fontSize << "' font-family='Arial, Helvetica, sans-serif'>" << std::setprecision(2) << maxX * ratio << "</text>";
    }
}

void svg::chart_yaxis_labels(std::ofstream &out, double maxY)
{
    int x = padding - fontSize * 0.5;
    for (int i = 1; i < numHorizontalGuides + 1; i++)
    {
        double ratio = (i / (double)numHorizontalGuides);
        int y = chartHeight - chartHeight * ratio + padding + fontSize / 2;
        out << "<text x='" << x << "' y='" << y << "' font-size='" << fontSize << "' font-family='Arial, Helvetica, sans-serif' text-anchor='end'>" << std::setprecision(2) << maxY * ratio << "</text>";
    }
}

void svg::chart_vertical_guides(std::ofstream &out)
{
    int startY = padding;
    int endY = height - padding;

    for (int i = 0; i < numVerticalGuides; i++)
    {
        double ratio = (i + 1) / (double)numVerticalGuides;
        int x = padding + ratio * (width - padding * 2);

        out << "<polyline fill='none' stroke='#ccc' strokeWidth='.5' points='" << x << "," << startY << " " << x << "," << endY << "' />\n";
    }
}

void svg::chart_horizontal_guides(std::ofstream &out)
{
    int startX = padding;
    int endX = width - padding;

    for (int i = 0; i < numHorizontalGuides; i++)
    {
        double ratio = (i + 1) / (double)numHorizontalGuides;
        int y = chartHeight - chartHeight * ratio + padding;

        out << "<polyline fill='none' stroke='#ccc' strokeWidth='.5' points='" << startX << "," << y << " " << endX << "," << y << "' />\n";
    }
}

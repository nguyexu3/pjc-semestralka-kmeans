#pragma once

class point
{
public:
    point(double x, double y) : x(x), y(y), cluster(-1){};

    double x, y;
    int cluster;

    double distance(const point &p) const;

    friend bool operator==(const point &a, const point &b);
    friend bool operator!=(const point &a, const point &b);
};
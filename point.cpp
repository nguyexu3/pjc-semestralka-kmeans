#include "point.hpp"
#include <cmath>

double point::distance(const point &p) const
{
    double res = std::pow((p.x - x), 2) + std::pow((p.y - y), 2);
    return std::sqrt(res);
}

bool operator==(const point &a, const point &b)
{
    return (a.x == b.x) && (a.y == b.y);
}

bool operator!=(const point &a, const point &b)
{
    return !(a == b);
};
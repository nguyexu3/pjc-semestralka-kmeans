#include "kmeans.hpp"
#include "svg.hpp"
#include "point.hpp"
#include <fstream>
#include <iostream>
#include <regex>
#include <future>
#include <chrono>
#include <string>

template <typename TimePoint>
std::chrono::milliseconds get_milis(TimePoint tp)
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void parse_line(std::vector<point> &data, std::string line)
{
    std::vector<double> pair;
    std::string num;
    for (const char &c : line)
    {
        if (c == 45 || c == 46 || (c > 47 && c < 58))
        {
            num += c;
        }
        else if (num != "")
        {
            pair.push_back(std::stod(num));
            num = "";
        }
    }

    if (num != "")
    {
        pair.push_back(std::stod(num));
    }

    if (pair.size() != 2)
    {
        throw std::runtime_error("Invalid data");
    }

    data.push_back(point(pair[0], pair[1]));
}

std::vector<point> data_from_file(const std::string path)
{
    std::vector<point> data;
    std::ifstream file;
    std::string line;

    file.open(path);
    if (!file.is_open())
    {
        throw std::runtime_error("File not found");
    }
    while (std::getline(file, line))
    {
        parse_line(data, line);
    }
    file.close();

    return data;
}

void print_help()
{
    std::cout << "Options:\n"
              << "-mt    multithread on\n"
              << "-i    specify input file (required)\n"
              << "-k    specify the number of clusters (required)\n";
}

void evaluate_genereate_kmeans(std::string f, const int k)
{
    std::vector<point> data = data_from_file(f);
    kmeans kmeans;
    kmeans.evaluate(k, data);
    svg svg;
    svg.generate(kmeans.classifications, kmeans.centroids, f.substr(0, f.length() - 4) + ".svg");
}

int main(int argc, char const *argv[])
{
    int k = 0;
    bool multi_thread = false;
    std::string input = "";

    try
    {
        for (int i = 1; i < argc; i++)
        {
            if (!strcmp(argv[i], "--help"))
            {
                print_help();
                return 0;
            }
            if (!strcmp(argv[i], "-mt"))
            {
                multi_thread = true;
            }
            else if (!strcmp(argv[i], "-i"))
            {
                if (i + 1 >= argc)
                {
                    throw std::runtime_error("No input path argument");
                }
                input = argv[++i];
            }
            else if (!strcmp(argv[i], "-k"))
            {
                if (i + 1 >= argc)
                {
                    throw std::runtime_error("No cluster number argument");
                }
                std::size_t endpos = 0;
                k = std::stoi(argv[++i], &endpos);
            }
            else
            {
                throw std::runtime_error("Invalid command");
            }
        }

        if (argc > 1 && k != 0 && input != "")
        {
            auto start = std::chrono::high_resolution_clock::now();

            std::regex regex{R"([,]+)"};
            std::sregex_token_iterator it{input.begin(), input.end(), regex, -1};
            std::vector<std::string> files{it, {}};
            if (multi_thread)
            {
                std::vector<std::future<void>> futures;
                for (const std::string f : files)
                {
                    futures.push_back(std::async(&evaluate_genereate_kmeans, f, k));
                }
            }
            else
            {
                for (const std::string f : files)
                {
                    evaluate_genereate_kmeans(f, k);
                }
            }

            std::cout << "Evaluation and generation ended.. " << std::endl;
            std::cout << "Time: " << get_milis(std::chrono::high_resolution_clock::now() - start).count() << std::endl;
        }
        else
        {
            throw std::runtime_error("Missing commands");
        }
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch (const std::invalid_argument &e)
    {
        std::cerr << "Invalid number" << std::endl;
    }
}

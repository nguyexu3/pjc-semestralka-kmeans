#include "kmeans.hpp"
#include <iostream>
#include <float.h>

void kmeans::evaluate(const int k, std::vector<point> data)
{
    // initlize centroids with first k points of of data, we could randomise it too
    std::vector<point> centroids;
    for (int i = 0; i < k; i++)
    {
        centroids.push_back(data[i]);
    }

    for (int i = 0; i < max_iteration; i++)
    {
        // asign a cluster for the closest point
        for (point &p : data)
        {
            double minDistance = DBL_MAX;
            for (int c = 0; c < k; c++)
            {
                double distance = p.distance(centroids[c]);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    p.cluster = c;
                }
            }
        }

        // calculate the mean position for centroids
        std::vector<int> nPoints;
        std::vector<point> oldCentroids = centroids;
        for (int c = 0; c < k; c++)
        {
            nPoints.push_back(0);
            centroids[c].x = 0;
            centroids[c].y = 0;
        }
        for (const point &p : data)
        {
            nPoints[p.cluster] += 1;
            centroids[p.cluster].x += p.x;
            centroids[p.cluster].y += p.y;
        }
        for (int c = 0; c < k; c++)
        {
            centroids[c].x = centroids[c].x / nPoints[c];
            centroids[c].y = centroids[c].y / nPoints[c];
        }

        // assest if the centroids are moving
        bool isConverting = true;
        for (int c = 0; c < k; c++)
        {
            if (centroids[c] != oldCentroids[c])
            {
                isConverting = false;
                break;
            }
        }
        if (isConverting)
        {
            break;
        }
    }

    this->centroids = centroids;
    this->classifications = data;
};
